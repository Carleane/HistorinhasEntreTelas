package com.example.carleanesousa.projetohistoriasentretelas;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.example.carleanesousa.projetohistoriasentretelas.MainActivity;

public class QuartaActivity extends AppCompatActivity {

    TextView mostrarTexto1;
    TextView mostrarTexto2;
    Button btn4;
    String valFromAct1;
    String valFromAct2;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quarta);

        setRequestedOrientation((ActivityInfo.SCREEN_ORIENTATION_PORTRAIT));


        btn4 = (Button) findViewById(R.id.button4);

        mostrarTexto1 = (TextView) findViewById(R.id.text11);
        valFromAct1 = getIntent().getExtras().getString("value");
        mostrarTexto1.setText(valFromAct1);

        mostrarTexto2 = (TextView) findViewById(R.id.text13);
        valFromAct2 = getIntent().getExtras().getString("value");
        mostrarTexto2.setText(valFromAct2);
    }
    public void btnClick4(View v){
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
        finish();
    }

}
