package com.example.carleanesousa.projetohistoriasentretelas;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class TerceiraActivity extends AppCompatActivity {

    TextView texto;
    Button btn3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terceira);

        setRequestedOrientation((ActivityInfo.SCREEN_ORIENTATION_PORTRAIT));

        btn3= (Button) findViewById(R.id.button3);
        texto = (TextView) findViewById(R.id.text7);
    }

    public void btnClick3(View v) {
        Intent i = new Intent(this, QuartaActivity.class);
        startActivity(i);
        finish();

    }
}
